package rs.ac.singidunum.fir.pj.library.controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import rs.ac.singidunum.fir.pj.library.entities.Customer;
import rs.ac.singidunum.fir.pj.library.repositories.CustomerRepository;

@RestController
public class CustomerController {

	@Autowired
	private CustomerRepository repository;
	
	@GetMapping("/customer")
	public List<Customer> getAllCustomers(){
		return (List<Customer>)repository.findAll();
	}
	
	@GetMapping("/customer/{id}")
	public Customer findCustomer(@PathVariable int id){
		return repository.findById(id).orElse(null);
	}
	
	@PostMapping("/customer")
	public Customer createCustomer(@RequestBody Customer customer){
		return repository.save(customer);
	}
	
	@DeleteMapping("/customer/{id}")
	public void deleteCustomer(@PathVariable int id){
		repository.deleteById(id);
	}
	
	@PutMapping("/customer/{id}")
	public Customer updateCustomer(@PathVariable int id, @RequestBody Customer customer){
		return repository.save(customer);
	}

}
