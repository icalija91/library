package rs.ac.singidunum.fir.pj.library.repositories;

import org.springframework.data.repository.CrudRepository;

import rs.ac.singidunum.fir.pj.library.entities.Customer;

public interface CustomerRepository extends CrudRepository<Customer, Integer>{
	
	//Customer findByString(String stringCode);

}
