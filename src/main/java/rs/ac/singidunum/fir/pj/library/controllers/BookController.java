package rs.ac.singidunum.fir.pj.library.controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import rs.ac.singidunum.fir.pj.library.repositories.BookRepository;
import rs.ac.singidunum.fir.pj.library.entities.*;

@RestController
public class BookController {
	
	@Autowired
	private BookRepository repository;
	
	@GetMapping("/book")
	public List<Book> getAllBooks(){
		return (List<Book>)repository.findAll();
		
	}
	
	@GetMapping("/book/{id}")
	public Book getBook(@PathVariable int id){
		return repository.findById(id).orElse(null);
	}
	
	@PostMapping("/book")
	public Book addBook(@RequestBody Book book){
		return repository.save(book);
	}
	
	@DeleteMapping("/book/{id}")
	public void deleteBook(@PathVariable int id){
		repository.deleteById(id);
		
	}
	
	@PutMapping("/book/{id}")
	public Book updateBook(@PathVariable int id, @RequestBody Book book){
		return repository.save(book);
	}
	
	@GetMapping("/book/count")
	public long getNumberOfBooks(){
		return repository.count();
	}


}
