package rs.ac.singidunum.fir.pj.library.repositories;

import org.springframework.data.repository.CrudRepository;

import rs.ac.singidunum.fir.pj.library.entities.Book;

public interface BookRepository extends CrudRepository<Book, Integer>{
	
	//Book findByString(String stringCode);

}
