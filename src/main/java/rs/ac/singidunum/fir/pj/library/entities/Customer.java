package rs.ac.singidunum.fir.pj.library.entities;

import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;

@Entity(name="customer")
public class Customer {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int customer_id;
	
	private String name;
	
	@Column(name = "last_name")
	private String lastName;
	
	private String address;
	
	private String registration_date;
	
	@OneToMany(mappedBy= "customer", cascade = CascadeType.ALL, fetch = FetchType.LAZY)
	private Set<Book> books;

	
	public Customer(){
		
	}

	public Customer(int customer_id, String name, String lastName, String address, String registration_date) {
		this.customer_id = customer_id;
		this.name = name;
		this.lastName = lastName;
		this.address = address;
		this.registration_date = registration_date;
	}

	public int getCustomer_id() {
		return customer_id;
	}

	public void setCustomer_id(int customer_id) {
		this.customer_id = customer_id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getRegistration_date() {
		return registration_date;
	}

	public void setRegistration_date(String registration_date) {
		this.registration_date = registration_date;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	

}
